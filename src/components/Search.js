import React,{useState} from 'react'
import axios from 'axios'
import Result from "./Result"


function Search()
{
    const [state,SetState] = useState({
        s:"",data:[]
    })

    function handleChange(e)
    {
        let s = e.target.value
        SetState(prevState => {
            return {...prevState, s : s}
            }   
        )
    }
    const API = "http://www.omdbapi.com/?i=tt3896198&apikey=948f829e"
    function search(e)
    {  
        if(e.key === 'Enter')
        {
            axios(API+"&s="+state.s).then(({ data }) => {
                let results = data.Search;
                SetState(prevState=>{
                    return{...prevState,data:results}}  )
            })
        }
    }

    return(
        <div>
            <input 
                className = "search-bar"
                type = "text" 
                placeholder="Search for a movie ...."
                onChange = {handleChange}
                onKeyPress = {search}
            />
            <Result data = {state.data}/>
            
        </div>
    )
}

export default Search