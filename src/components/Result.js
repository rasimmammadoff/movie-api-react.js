import React from 'react'


function Result({ data }) {
    let showdata = (<h3 className="not-found">Nothing found</h3>)
    if (data) {
        showdata = (data.map(result => (
            <li className="list-item">
                <img src={result.Poster} alt="Img Not Found" />
                <h3 className='title'>{result.Title}</h3>
            </li>
        )))
    }
    return (
        <div>
            <ul className="list">{showdata}</ul>
        </div>
    )
}
export default Result